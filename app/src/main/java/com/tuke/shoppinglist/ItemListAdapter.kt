package com.tuke.shoppinglist

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

class ItemListAdapter(private val items: MutableList<String>): RecyclerView.Adapter<ItemListAdapter.ViewHolder>(){

    interface ItemsChangedListener{
        fun onItemAdded(item: String)
        fun onItemRemoved(item: String, position: Int)
    }

    var itemsChangedListener: ItemsChangedListener? = null
    fun setItemsListener(listener: ItemsChangedListener){
        itemsChangedListener = listener
    }

    class ViewHolder(itemView: View, parent: ItemListAdapter) : RecyclerView.ViewHolder(itemView){
        val itemLabel: TextView = itemView.findViewById(R.id.itemText)
        private val removeButton: Button = itemView.findViewById(R.id.remove_button)

        init {
            removeButton.setOnClickListener {
                val itemName = parent.items.removeAt(adapterPosition)
                parent.notifyItemRemoved(adapterPosition)
                parent.notifyDataSetChanged()
                parent.itemsChangedListener?.onItemRemoved(itemName, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_list_row, parent, false)
        return ViewHolder(view, this)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemLabel.text = items[position]
    }

    fun swap(indexA: Int, indexB: Int): Boolean{
        try {
            items[indexA] = items[indexB].also { items[indexB] = items[indexA] }
        }catch(e: RuntimeException){
            // Does it matter which exception happened?
            return false
        }
        return true
    }

    fun removeItem(itemIndex: Int){
        val removedItem = items.removeAt(itemIndex)
        Log.d("ItemListAdapter", "Removing $removedItem")
        notifyItemRemoved(itemIndex)
        notifyDataSetChanged()
        itemsChangedListener?.onItemRemoved(removedItem, itemIndex)
    }

    fun addItem(item: String){
        items.add(item)
        notifyDataSetChanged()
        itemsChangedListener?.onItemAdded(item)
    }

    fun addSet(itemSet: Set<String>){
        for(item in itemSet){
            addItem(item)
        }
        notifyDataSetChanged()
    }



}