package com.tuke.shoppinglist

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button


class MainActivity : AppCompatActivity() {

    private lateinit var preferences: SharedPreferences

    // The list of the items and their adapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: ItemListAdapter
    private lateinit var itemList: MutableList<String>

    // Input field and it's adapter
    private lateinit var itemInput: AutoCompleteTextView
    private lateinit var autoAdapter: ArrayAdapter<String>

    // Button to add items to the view
    private lateinit var addButton: Button

    // Set for words currently known by the app
    private lateinit var knownWords: MutableSet<String>
    // Set for words on the list
    val currentList: MutableSet<String> = mutableSetOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preferences = applicationContext.getSharedPreferences(getString(R.string.preference_key), Context.MODE_PRIVATE)

        // Prepare the Recycler View and its components
        viewManager = LinearLayoutManager(this)
        itemList = mutableListOf()
        viewAdapter = ItemListAdapter(itemList)
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
        ItemTouchHelper(MyTouchCallback(viewAdapter,0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT))
            .attachToRecyclerView(recyclerView)

        // Restore the items from last session
        restorePreviousView()

        // Setup add button to update the known words and add items to the RecyclerView
        addButton = findViewById(R.id.addButton)
        addButton.setOnClickListener{
            val item = itemInput.text.toString()
            viewAdapter.addItem(item)
            viewAdapter.notifyDataSetChanged()
            updateKnownWords(item)
            itemInput.text.clear()
        }

        // Update the cache after item has been added/removed
        // The cache could also be updated every time the activity is paused or stopped,
        // but I found this method more fool(=me) proof.
        viewAdapter.setItemsListener(object: ItemListAdapter.ItemsChangedListener{
            override fun onItemAdded(item: String){
                currentList.add(item)
                preferences.edit().putStringSet(getString(R.string.saved_view), currentList).apply()
                Log.d("MainActivity", "$item added to cache")
            }
            override fun onItemRemoved(item: String, position: Int) {
                currentList.remove(item)
                preferences.edit().putStringSet(getString(R.string.saved_view), currentList).apply()
                Log.d("MainActivity", "$item removed from cache")
            }
        })
    }

    override fun onResume() {
        super.onResume()
        Log.d("MainActivity", "Resuming the activity")
        prepareAutocomplete()
    }

    /**
     * Fetches the known words from the shared preferences and sets the input field to suggest these words.
     */
    private fun prepareAutocomplete(){
        Log.d("MainActivity", "Preparing autocomplete")
        itemInput = findViewById(R.id.input_field)
        knownWords = preferences.getStringSet(getString(R.string.item_dictionary), mutableSetOf()) ?: mutableSetOf()
        autoAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, knownWords.toMutableList())
        itemInput.setAdapter(autoAdapter)
        itemInput.threshold = 1
        autoAdapter.notifyDataSetChanged()
    }

    /**
     * If the word is not known (i.e. not found in knownWords),
     * then the word is added to the known words and to the autocomplete's adapter.
     */
    private fun updateKnownWords(word: String){
        if(word !in knownWords){
            knownWords.add(word)
            autoAdapter.add(word)
            autoAdapter.notifyDataSetChanged()
            preferences.edit().putStringSet(getString(R.string.item_dictionary), knownWords).apply()
        }
    }

    /**
     * Restores the stored list of words to the recycler view.
     */
    private fun restorePreviousView(){
        val oldItems = preferences.getStringSet(getString(R.string.saved_view), mutableSetOf())
        oldItems?.forEach { item -> currentList.add(item) }
        viewAdapter.addSet(currentList)
        viewAdapter.notifyDataSetChanged()
    }


    /**
     * Adds the menu bar to the activity.
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menubar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId){
            R.id.menu_dictionary -> {
                val intent = Intent(this, DictionaryActivity::class.java).apply{}
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

