package com.tuke.shoppinglist

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper

class MyTouchCallback(itemListAdapter: ItemListAdapter, dragDirs: Int, swipeDirs: Int): ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs){

    private var adapter = itemListAdapter

    override fun onMove(recyclerView: RecyclerView, movingViewHolder: RecyclerView.ViewHolder, targetViewHolder: RecyclerView.ViewHolder): Boolean { return false }

    override fun onSwiped(swipedViewHolder: RecyclerView.ViewHolder, direction: Int) {
        adapter.removeItem(swipedViewHolder.adapterPosition)
    }
}