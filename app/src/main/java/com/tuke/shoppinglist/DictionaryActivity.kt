package com.tuke.shoppinglist

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

class DictionaryActivity : AppCompatActivity() {

    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: ItemListAdapter
    private lateinit var itemList: MutableList<String>
    private lateinit var preferences: SharedPreferences
    private val knownWords: MutableSet<String?> = mutableSetOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dictionary)
        // Get Shared Preferences
        preferences = applicationContext.getSharedPreferences(getString(R.string.preference_key), Context.MODE_PRIVATE)
        val itemDictionary = preferences.getStringSet(getString(R.string.item_dictionary), null) ?: mutableSetOf()
        for(word in itemDictionary)
            knownWords.add(word)
        // Prepare UI components
        viewManager = LinearLayoutManager(this)
        itemList = mutableListOf()
        viewAdapter = ItemListAdapter(itemList)
        findViewById<RecyclerView>(R.id.dictionary_view).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
        populateView()
        viewAdapter.setItemsListener(object: ItemListAdapter.ItemsChangedListener{
            override fun onItemAdded(item: String) {}
            override fun onItemRemoved(item: String, position: Int) {
                knownWords.remove(item)
                preferences.edit().putStringSet(getString(R.string.item_dictionary), knownWords).apply()
            }
        })
    }

    private fun populateView(){
        val values = knownWords.toList().sortedBy { it }
        for (word in values){
            if(word != null) viewAdapter.addItem(word)
        }
        viewAdapter.notifyDataSetChanged()
    }
}
