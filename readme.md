# ShoppingList

Once upon a time I got fedup with the free shopping list apps found from the play store. I just wanted a simple add free app, where I could add and remove items at whim to a list.

I also wanted an app, which would give me proper suggestions, ie. if I type a letter b, I want my app to given autocomplete suggestion for things like bread, bananas or other food items I usually buy, not particles such as be, but or by.

So I made this dead simple and idiot proof shopping list app which has  mindblowing features such as adding and removing items and an autosuggestions based on items you have previosly added to your list. The app also supports removing items from the app level dictionary, in case the user has mistyped the word or just doesn't want to see certain items ever again.